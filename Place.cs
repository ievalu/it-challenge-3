namespace IT_CHALLENGE__3 {
    public class Place {

        public Place (double freePlaces, double takenPlaces) {
            this.FreePlaces = freePlaces;
            this.TakenPlaces = takenPlaces;

        }
        
        public double FreePlaces { get; set; }
        public double TakenPlaces { get; set; }
    }
}