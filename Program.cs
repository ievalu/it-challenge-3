﻿using System;
using System.Collections.Generic;

namespace IT_CHALLENGE__3
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser myParser = new Parser(
                "https://raw.githubusercontent.com/vilnius/darzeliai/master/data/Darzeliu%20galimu%20priimti%20ir%20lankantys%20vaikai2018.csv",
                "kindergardeninfo.txt");
            List<KinderGarden> kinderGardenInfo = myParser.Parse();
            Statistics.LowestChildCount(kinderGardenInfo);
            Statistics.HighestChildCount(kinderGardenInfo);
            Statistics.LangWithMostFreePlaces(kinderGardenInfo);
            Statistics.GroupKinderGardens(kinderGardenInfo);
        }
    }
}
