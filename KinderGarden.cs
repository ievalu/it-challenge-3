namespace IT_CHALLENGE__3 {
    public class KinderGarden {

        public KinderGarden (int kinderGardenId, string kinderGardenName, int typeId, string typeLabel, int languageId, string languageLabel, int childCount, int freeSpaces) {
            this.KinderGardenId = kinderGardenId;
            this.KinderGardenName = kinderGardenName;
            this.TypeId = typeId;
            this.TypeLabel = typeLabel;
            this.LanguageId = languageId;
            this.LanguageLabel = languageLabel;
            this.ChildCount = childCount;
            this.FreeSpaces = freeSpaces;

        }

        public int KinderGardenId { get; set; }
        public string KinderGardenName { get; set; }
        public int TypeId { get; set; }
        public string TypeLabel { get; set; }
        public int LanguageId { get; set; }
        public string LanguageLabel { get; set; }
        public int ChildCount { get; set; }
        public int FreeSpaces { get; set; }
    }
}