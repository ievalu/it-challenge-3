using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace IT_CHALLENGE__3
{
    public class Statistics
    {
        public static void LowestChildCount(List<KinderGarden> kinderGardenInfoList)
        {
            int min = kinderGardenInfoList.Min(kinderGarden => kinderGarden.ChildCount);
            List<KinderGarden> minValList = kinderGardenInfoList.Where(
                kinderGarden => kinderGarden.ChildCount == min).ToList();

            List<string> linesToWrite = new List<string>();
            foreach (KinderGarden kinderGarden in minValList)
            {
                linesToWrite.Add(FormatLine(kinderGarden));
            }
            linesToWrite.Insert(0, min.ToString());
            File.WriteAllLines(Path.Combine(Environment.CurrentDirectory, 
                "output",
                "LowestChildCount.txt"), 
                linesToWrite.ToArray());
            Console.WriteLine("Lowest child count: " + min);
        }
        internal static void HighestChildCount(List<KinderGarden> kinderGardenInfoList)
        {
            List<string> linesToWrite = new List<string>();

            int max = kinderGardenInfoList.Max(kinderGarden => kinderGarden.ChildCount);
            List<KinderGarden> minValList = kinderGardenInfoList.Where(
                kinderGarden => kinderGarden.ChildCount == max).ToList();
        
            foreach (KinderGarden kinderGarden in minValList)
            {
                linesToWrite.Add(FormatLine(kinderGarden));
            }
            linesToWrite.Insert(0, max.ToString());
            File.WriteAllLines(Path.Combine(Environment.CurrentDirectory, 
                "output", 
                "HighestChildCount.txt"), 
                linesToWrite.ToArray());
            Console.WriteLine("Highest child count: " + max);
        }

        private static string NumbersFromTypeLabel(string typeLabel){
            string result = "";
            Regex nuoIkiFullRegex = new Regex(@"^Nuo [\d,]+ iki [\d,]+ metų$");
            Regex nuoRegex = new Regex(@"^Nuo [\d,]+m.$");
            // Regex ikiRegex = new Regex(@"^Iki [\d,]+ metų$");
            // Matching with this Regex in else block,
            // assuming there is no other pattern
            Regex digitRegex = new Regex(@"[\d,]+");
            if(nuoIkiFullRegex.IsMatch(typeLabel)){
                List<String> numbers = digitRegex.Matches(typeLabel)
                                                 .Select(match => match.Value)
                                                 .ToList();
                result = numbers[0] + "-" + numbers[1];
            }
            else if(nuoRegex.IsMatch(typeLabel)){
                List<String> numbers = digitRegex.Matches(typeLabel)
                                                 .Select(match => match.Value)
                                                 .ToList();
                result = numbers[0] + "-" + numbers[0] + "+";
            }
            else {
                List<String> numbers = digitRegex.Matches(typeLabel)
                                                 .Cast<Match>()
                                                 .Select(match => match.Value)
                                                 .ToList();
                result = "0" + "-" + numbers[0];
            }
            return result;
        } 
        private static string FormatLine(KinderGarden kinderGarden)
        {
            return (kinderGarden.KinderGardenName.Substring(0,3) + "_" +
                NumbersFromTypeLabel(kinderGarden.TypeLabel) + "_" +
                kinderGarden.LanguageLabel.Substring(0, 4));
        }
        public static void LangWithMostFreePlaces(List<KinderGarden> kinderGardenInfoList)
        {
            Dictionary<string, Place> freePlaces = new Dictionary<string, Place>();
            foreach (KinderGarden kinderGarden in kinderGardenInfoList)
            {
                if(!(freePlaces.ContainsKey(kinderGarden.LanguageLabel))){
                    freePlaces[kinderGarden.LanguageLabel] = new Place(0.0, 0.0);
                }
                freePlaces[kinderGarden.LanguageLabel].FreePlaces += kinderGarden.FreeSpaces;
                freePlaces[kinderGarden.LanguageLabel].TakenPlaces += kinderGarden.ChildCount;
            }
            double max = 0.0;
            string[] toFile = new string[] {"0", ""};
            foreach(string language in freePlaces.Keys){
                double freePlacesPercent = Math.Round(((freePlaces[language].FreePlaces / (freePlaces[language].FreePlaces + freePlaces[language].TakenPlaces)) * 100), 2);
                if(freePlacesPercent > max){
                    toFile[0] = freePlacesPercent.ToString();
                    toFile[1] = language;
                }
            }
            File.WriteAllLines(Path.Combine(Environment.CurrentDirectory, 
                "output", 
                "MostFreeSpaces.txt"), 
                toFile);
        }
        public static void GroupKinderGardens(List<KinderGarden> kinderGardenInfoList)
        {
            var grouped = kinderGardenInfoList
                .Where(kinderGarden => (2 <= kinderGarden.FreeSpaces) && (kinderGarden.FreeSpaces <= 4))
                .GroupBy(kinderGarden => kinderGarden.KinderGardenName)
                .Select(group => new {
                    Name = group.Key,
                    Count = group.Count()
                })
                .OrderByDescending(kinderGarden => kinderGarden.Name)
                .ToArray();

            string[] toFile = new string[grouped.Length];
            for(int i=0;i<grouped.Length;i++){
                toFile[i] = grouped[i].Name + ": " + grouped[i].Count;
            }
            File.WriteAllLines(Path.Combine(
                Environment.CurrentDirectory, 
                "output", 
                "GroupedFreeSpaces.txt"), 
                toFile);
        }
    }
}