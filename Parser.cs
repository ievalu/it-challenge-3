using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace IT_CHALLENGE__3
{
    public class Parser
    {
        public Parser(string urlToDownload, string fileTarget){
            this.url = urlToDownload;
            this.fileName = fileTarget;
        }

        private string url;
        private string fileName;

        public List<KinderGarden> Parse(){
            List<KinderGarden> parsedInfo = new List<KinderGarden>();
            Download();
            List<string> lines = Read();
            foreach(string line in lines){
                string[] splitLine = line.Split(";");
                parsedInfo.Add(new KinderGarden(
                    Int32.Parse(splitLine[0]),
                    splitLine[1].Trim('"'),
                    Int32.Parse(splitLine[2]),
                    splitLine[3].Trim('"'),
                    Int32.Parse(splitLine[4]),
                    splitLine[5],
                    Int32.Parse(splitLine[6]),
                    Int32.Parse(splitLine[7])));
            }
            return parsedInfo;
        }
        public void Download(){
            using (var client = new WebClient())
            {
                client.DownloadFile(url, fileName);
            }
        }
        public List<string> Read(){
            List<string> lines = new List<string>(); 
            string line;
            StreamReader reader = new StreamReader(fileName);
            reader.ReadLine();
            while((line = reader.ReadLine()) != null){
                lines.Add(line);
            }
            return lines;
        }
    }
}